# Description
```
atool v2.0

atool is for unpacking and repacking the android boot.img
and recovery.img easily.

Note:
	atool has been tested on ubuntu linux
	should work on most debian based linux distros
```

# Usage
```
1. Place your image file in the atool root directory.
2. Open terminal to the atool root directory and run
the following command:

$ ./atool
```

![image](tools/menu.png)

# atool Author
- [GameTheory](https://bitbucket.org/GameTheory-)

**Resources:**

- [mkbootfs](https://github.com/GameTheory-/mkbootimg)

- [abootimg](http://bazaar.launchpad.net/~ubuntu-branches/ubuntu/trusty/abootimg/trusty/files)
